%save pointcloud as .pcd
function ret = pcdwrite(filename, pc)
  
  fid = fopen(filename, 'w');
  fprintf(fid, "# .PCD v0.7 - Point Cloud Data file format\n");
  fprintf(fid, "VERSION 0.7\n");
  fprintf(fid, "FIELDS x y z\n");
  fprintf(fid, "SIZE 4 4 4\n");
  fprintf(fid, "TYPE F F F\n");
  fprintf(fid, "COUNT 1 1 1\n");
  fprintf(fid, "WIDTH %d\n", pc.width);
  fprintf(fid, "HEIGHT  %d\n", pc.height);
  fprintf(fid, "VIEWPOINT %d %d %d %d %d %d %d\n", pc.viewpoint(:));
  fprintf(fid, "POINTS %d\n", pc.points);
  fprintf(fid, "DATA ascii\n"); 
  
  for i = 1:size(pc.data)(1)
    fprintf(fid, "%s %s %s\n", num2str(pc.data(i, 1)), num2str(pc.data(i, 2)), num2str(pc.data(i, 3)));
  endfor
  
  fclose(fid);
  
  ret = 1
endfunction
