% show pointcloud
function ret = pcdshow(PC)
  plot3(PC.data(:,1), PC.data(:,2), PC.data(:,3), 'b.');
  xlabel('x');
  ylabel('y');
  zlabel('z');
  axis equal;
endfunction
