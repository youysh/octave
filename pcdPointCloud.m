function PT = pcdPointCloud(x, y, z)
  PT.data(:,1) = x;
  PT.data(:,2) = y;
  PT.data(:,3) = z;
  PT.width = size(x)(2);
  PT.height = 1;
  PT.points = size(x)(2);
  PT.viewpoint = [0 0 0 1 0 0 0];
  PT.fields = strsplit('x y z');
endfunction
